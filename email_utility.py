import yagmail
import config

def sent_email():
    user = ''
    app_password = '' # a token for gmail
    to = ''

    subject = 'test subject 1'
    content = ['mail body content',config.WORKBOOK_NAME]

    with yagmail.SMTP(user, app_password) as yag:
        yag.send(to, subject, content)
        print('Sent email successfully')
import pandas as pd
import config as config
from preprocessing import Preprocessing
from phrase_chunking import PhraseChunking
import os
import ast
import json


class Write:


    def __init__(self,theme_list):
        self.data = pd.read_csv(config.CRAWLED_DATA_PATH)
        self.preprocessing = Preprocessing()
        self.phraseChunking = PhraseChunking()
        self.theme_list = theme_list
        

    def cleaning(self):
        #data['title'] = data['description'].apply(lambda x: x + 1)
        self.data['cleaned_title'] = self.data.apply(lambda x: self.preprocessing.clean_text(x['title']), axis=1)
        self.data['cleaned_description'] = self.data.apply(lambda x: self.preprocessing.clean_text(str(x['description'])), axis=1)
        

    def get_images(self):
        self.cleaning()
        theme_lists = []
        image_list = []
        for theme in self.theme_list:
            filtered_df = self.data[self.data['cleaned_description'].str.contains(theme) | self.data['cleaned_title'].str.contains(theme)]
            if len(filtered_df)>20:
                theme_lists.append(theme)
                img_list = []
                for index,row in filtered_df.iterrows():
                    try:
                        img_list.append(ast.literal_eval(row['image_url_list'])[0])
                    except:
                        img_list.append(row['image_url_list'])
                image_list.append(img_list[:10])
        print(image_list)
        return theme_lists, image_list
    
    
    def write_to_csv(self, theme_lists, image_list):
        empty_df = pd.DataFrame(columns = ['Remark (OK/ Reject)', 'Must have/ OK - Recommended', 'Already existing/ New','Type','Rename'])
        image_df = pd.DataFrame(image_list, columns = ['image' + str(i+1) for i in range(10)])
        themes_df = pd.DataFrame({'Themes':theme_lists})
        frames = [empty_df, themes_df, image_df]

        result = pd.concat(frames, axis = 1)
        result.to_csv(config.RANKED_THEME_OUTPUT_PATH, index=False, header=True)
        print("write is successful")
        
    def write_to_json(self,theme_lists):
        
        with open(config.ALREADY_EXISTING_THEMES,'r') as f:
            themes = json.load(f)["already_existing_list"]
        
        
        json_obj = {}
        json_obj["already_existing_list"] = list(set(theme_lists + themes))
        with open(config.ALREADY_EXISTING_THEMES,"w") as f:
            json.dump(json_obj,f)
        print("already existing list updated")
    
    
    def main(self):
        theme_lists, image_list = self.get_images()
        self.write_to_csv(theme_lists,image_list)
        self.write_to_json(theme_lists)
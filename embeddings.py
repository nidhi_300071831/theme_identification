import numpy as np
import tensorflow_hub as hub
import config as config
import logging
logging.basicConfig(level=logging.INFO, filename=config.LOGGER_PATH)



class UniversalSentEnc:


    def __init__(self):
        #self.embed = hub.load("https://tfhub.dev/google/universal-sentence-encoder-large/5")
        self.embed = hub.load(config.MODEL_PATH)


    def use_embedding(self, list_of_candidate, fashionwords):
        #embed = hub.load("https://tfhub.dev/google/universal-sentence-encoder-large/5")
        resulting_embedding = np.empty((0,512))
        for i in range(int(len(list_of_candidate)/10000)+1):
          query_embedding = self.embed(list_of_candidate[10000*i:10000*(i+1)])
          resulting_embedding = np.vstack((resulting_embedding, query_embedding))
          #logging.info(query_embedding.shape)
        logging.info("candidate embedding shape {a}".format(a=resulting_embedding.shape))
        logging.info("saving the candidate embeddings")
        with open(config.CANDIDATE_EMBEDDING_PATH, 'wb') as f:
            np.save(f, resulting_embedding)

        fashion_embedding = self.embed(fashionwords)
        logging.info("seed themes embedding shape {a}".format(a=fashion_embedding.shape))
        with open(config.SEED_THEMES_EMBEDDING_PATH, 'wb') as f:
            np.save(f, fashion_embedding)


    def create_embedding(self, expanded_set):
        #embed = hub.load("https://tfhub.dev/google/universal-sentence-encoder-large/5")
        expanded_set_embedding = self.embed(expanded_set)
        return expanded_set_embedding


import nltk
from nltk import word_tokenize, pos_tag
from nltk.corpus import wordnet
import nltk
from IPython.display import display
import logging
import config as config
logging.basicConfig(level=logging.INFO, filename=config.LOGGER_PATH)
nltk.download('punkt')
nltk.download('averaged_perceptron_tagger')



#word tokenizeing and part-of-speech tagger

class PhraseChunking:


    def __init__(self):
        self.lemmatizer = nltk.WordNetLemmatizer()


    def candidate_generation(self, sentence):
        document = sentence
        tokens = [nltk.word_tokenize(sent) for sent in [document]]
        postag = [nltk.pos_tag(sent) for sent in tokens][0]
        #{<RB.?>*<VB.?>*<JJ>*<VB.?>+<VB>?} # Verbs and Verb Phrases
        # Rule for NP chunk and VB Chunk
        grammar = r"""
            NBAR:
                {<NN.*|JJ>*<NN.*>}  # Nouns and Adjectives, terminated with Nouns


            NP:
                {<NBAR>}
                {<NBAR><IN><NBAR>}  # Above, connected with in/of/etc...

        """
        #Chunking
        cp = nltk.RegexpParser(grammar)

        # the result is a tree
        tree = cp.parse(postag)
        #logging.info(tree)
        terms = self.get_terms(tree)

        features = []
        for term in terms:
            _term = ''
            for word in term:
                _term += ' ' + word
            features.append(_term.strip())

        features = [f for f in features if len(f.split())>1 and len(f.split())<=4]
        features = list(set(features))
        return features


    def leaves(self, tree):
        """Finds NP (nounphrase) leaf nodes of a chunk tree."""
        for subtree in tree.subtrees(filter = lambda t: t.label() =='NP'):
            yield subtree.leaves()


    def get_word_postag(self, word):
        if pos_tag([word])[0][1].startswith('J'):
            return wordnet.ADJ
        if pos_tag([word])[0][1].startswith('V'):
            return wordnet.VERB
        if pos_tag([word])[0][1].startswith('N'):
            return wordnet.NOUN
        else:
            return wordnet.NOUN


    def normalise(self, word):
        """Normalises words to lowercase and stems and lemmatizes it."""
        word = word.lower()
        postag = self.get_word_postag(word)
        word = self.lemmatizer.lemmatize(word,postag)
        return word


    def get_terms(self, tree):
        for leaf in self.leaves(tree):
            #terms = [self.normalise(w) for w,t in leaf]
            terms = [w for w,t in leaf]
            yield terms

#phrase_chunking = PhraseChunking()
#features = phrase_chunking.candidate_generation("maternity dress , by asos design , for that thing you have to go to | plunge neck | cold shoulder design | pleated skirt | cowl back | maxi length | regular cut | designed to fit you from bump to baby")
#logging.info(features)

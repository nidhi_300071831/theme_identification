import nltk
from nltk.corpus import stopwords
from nltk.stem import WordNetLemmatizer
from nltk.tokenize import TweetTokenizer
from collections import Counter
import re, string
from nltk.tokenize import word_tokenize
import logging
import config as config
logging.basicConfig(level=logging.INFO, filename=config.LOGGER_PATH)
#from spacy.lang.en import English
import numpy as np
#nlp = English()
#tokenizer = nlp.tokenizer

nltk.download('wordnet')
nltk.download('stopwords')
nltk.download('punkt')




class Preprocessing:


    def __init__(self):
        self.tknzr = TweetTokenizer(strip_handles=True, reduce_len=True)
        self.wordnet_lemmatizer = WordNetLemmatizer()
        self.sw = stopwords.words("english")
        self.special_chars = [ '*', '~', '!', '$',
                         '%', '^', '|'
                         '&', '(', ')', '+',
                         '[', ']', '{', '}',
                         '/', '-', '<',
                         '>', '?', ':',
                         '`']


    def remove_emoji(self, text):

      emoji_pattern = re.compile("["
            u"\U0001F600-\U0001F64F"  # emoticons
            u"\U0001F300-\U0001F5FF"  # symbols & pictographs
            u"\U0001F680-\U0001F6FF"  # transport & map symbols
            u"\U0001F1E0-\U0001F1FF"  # flags (iOS)
            u"\U0001F1F2-\U0001F1F4"  # Macau flag
            u"\U0001F1E6-\U0001F1FF"  # flags
            u"\U0001F600-\U0001F64F"
            u"\U00002702-\U000027B0"
            u"\U000024C2-\U0001F251"
            u"\U0001f926-\U0001f937"
            u"\U0001F1F2"
            u"\U0001F1F4"
            u"\U0001F620"
            u"\u2640-\u2642"
            "]+", flags=re.UNICODE)
      text = emoji_pattern.sub(r'', text) # no emoji
      return text


    def remove_special_char_and_numbers(self, word):
        clean_word = ''
        for char in word:
            if char not in self.special_chars and not char.isdigit():
                clean_word += char
            elif char.isdigit():
                clean_word +=''
            else:
                clean_word += ' '
        #logging.info(" ".join(clean_word.split()))
        return " ".join(clean_word.split())


    def remove_hashtag_mentioned(self, text):
        entity_prefixes = ['@','#']
        '''for separator in string.punctuation:
            if separator not in entity_prefixes :
                text = text.replace(separator,' ')'''
        words = []
        for word in text.split():
            word = word.strip()
            if word:
                if word[0] not in entity_prefixes and word.startswith('u00')== False:
                    words.append(word)
        return ' '.join(words)


    def clean_text(self, text):
        
        if text is np.nan:
            return ""
    
        if not text:
            return ""

        text = self.remove_emoji(text)
        list_of_words = [i.lower() for i in self.tknzr.tokenize(text)]
        #list_of_words = [i.lower() for i in word_tokenize(text)]
        list_of_words = [self.remove_special_char_and_numbers(word) for word in list_of_words]
        #list_of_words = filter(lambda x: "pic.twitter" not in x and not "twitter.com" in x, list_of_words)
        list_of_words = filter(lambda x: "http" not in x, list_of_words)
        text =  " ".join(list_of_words)
        text = self.remove_hashtag_mentioned(text)
        return text


#preprocessing = Preprocessing()
#logging.info(preprocessing.clean_text("!qwer f*rty"))

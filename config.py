
EMBEDDING_MODEL = "USE"
MODEL_PATH = "/rapid_data/nidhi/theme_identification/use_model"
TOP_N = 200 #number of final themes required
TOP_K = 30 #number of themes to be considered for each seed
LOGGER_PATH = "/rapid_data/nidhi/horizontal_website_data/logger_output.log"

SEED_THEMES_PATH = "/rapid_data/nidhi/horizontal_website_data/western_themes.json"
ALREADY_EXISTING_THEMES = "/rapid_data/nidhi/horizontal_website_data/already_existing_themes.json"
CRAWLED_DATA_PATH = "/rapid_data/nidhi/horizontal_website_data/shein16.csv"
CANDIDATE_EMBEDDING_PATH = "/rapid_data/nidhi/horizontal_website_data/candidate_embedding.npy"
SEED_THEMES_EMBEDDING_PATH = "/rapid_data/nidhi/horizontal_website_data/seed_embedding.npy"
EXPANDED_SET_PATH = "/rapid_data/nidhi/horizontal_website_data/expanded_set_shein.json"


TIME_SERIES_MODEl = "SARIMAX"
TIME_SERIES_DATA_PATH = "/rapid_data/nidhi/horizontal_website_data/time_series_data.csv"
RANKED_THEME_OUTPUT_PATH = "/rapid_data/nidhi/horizontal_website_data/themes_output.csv"


#WORKBOOK_NAME = "/rapid_data/nidhi/horizontal_website_data/crawled_themes.xlsx"
#WORKSHEET_NAME = "Identified_themes"




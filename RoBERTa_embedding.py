import torch
import torch.optim as optim
import torch.nn as nn
import torch.nn.functional as F
from torch.autograd import Variable
from transformers import RobertaModel, RobertaTokenizer, RobertaForMaskedLM, RobertaTokenizerFast
from transformers import RobertaForSequenceClassification, RobertaConfig


# If there's a GPU available...
if torch.cuda.is_available():

    # Tell PyTorch to use the GPU.
    device = torch.device("cuda")

    print('There are %d GPU(s) available.' % torch.cuda.device_count())

    print('We will use the GPU:', torch.cuda.get_device_name(0))

# If not...
else:
    print('No GPU available, using the CPU instead.')
    device = torch.device("cpu")



tokenizer = RobertaTokenizerFast.from_pretrained('FashionRoBERTaBPETokenizer/')
config = RobertaConfig.from_pretrained("RoBERTa_trained_model/")
print("done")

config.output_hidden_states = True

model = RobertaForMaskedLM.from_pretrained("RoBERTa_trained_model/", config=config)

model.cuda(device)


def getBertEmbedding(sentence):
    if len(sentence)>0:
        input_ids = torch.tensor(tokenizer.encode(sentence, add_special_tokens=True,max_length=15,truncation=True)).unsqueeze(0).to(device)
        #print(input_ids)
        outputs = model(input_ids[:15])
        sequence_output = outputs[1][6]
        final_vector = torch.mean(sequence_output,dim=1)
        return final_vector.cpu().data.numpy()[0:]

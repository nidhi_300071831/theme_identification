import pandas as pd
import config as config
import openpyxl
from tqdm import tqdm
from openpyxl.utils import get_column_letter
from openpyxl.writer.excel import save_virtual_workbook
from openpyxl.drawing.image import Image
import PIL
import io
import urllib3
from preprocessing import Preprocessing
from phrase_chunking import PhraseChunking
import ast
from openpyxl import load_workbook
import os



class Write:


    def __init__(self,theme_list):
        self.data = pd.read_csv(config.CRAWLED_DATA_PATH)
        self.preprocessing = Preprocessing()
        self.phraseChunking = PhraseChunking()
        self.theme_list = theme_list
        

    def cleaning(self):
        #data['title'] = data['description'].apply(lambda x: x + 1)
        self.data['cleaned_title'] = self.data.apply(lambda x: self.preprocessing.clean_text(x['title']), axis=1)
        self.data['cleaned_description'] = self.data.apply(lambda x: self.preprocessing.clean_text(str(x['description'])), axis=1)


    def create_workbook_and_sheet(self):
        if os.path.exists(config.WORKBOOK_NAME):
            os.remove(config.WORKBOOK_NAME)
        
        wb = openpyxl.Workbook()
        wb.save(config.WORKBOOK_NAME)
        wb2 = load_workbook(config.WORKBOOK_NAME)
        wb2.create_sheet(config.WORKSHEET_NAME)
        #wb2.get_sheet_names()
        #wb2.remove_sheet("Sheet")
        std=wb2.get_sheet_by_name("Sheet")
        wb2.remove_sheet(std)
        wb2.save(config.WORKBOOK_NAME)


    def get_images(self):
        self.cleaning()
        theme_lists = []
        image_list = []
        for theme in self.theme_list:
            filtered_df = self.data[self.data['cleaned_description'].str.contains(theme) | self.data['cleaned_title'].str.contains(theme)]
            if len(filtered_df)>20:
                theme_lists.append(theme)
                img_list = []
                for index,row in filtered_df.iterrows():
                    try:
                        img_list.append(ast.literal_eval(row['image_url_list'])[0])
                    except:
                        img_list.append(row['image_url_list'])
                image_list.append(img_list[:10])
        return theme_lists, image_list

        
    def write_in_sheet(self,theme_list,image_list):
        self.create_workbook_and_sheet()
        wb = openpyxl.load_workbook(config.WORKBOOK_NAME)
        ws = wb[config.WORKSHEET_NAME]
        for row, i in tqdm(enumerate(theme_list)):
            column_cell = 'D'
            ws[column_cell+str(row+2)] = str(i)
            for j,img in enumerate(image_list[row]):

                http = urllib3.PoolManager()
                r = http.request('GET', img)
                image_file = io.BytesIO(r.data)
                img = openpyxl.drawing.image.Image(image_file)
                
                img.anchor = get_column_letter(j+5)  + str(row+2) # Or whatever cell location you want to use.
                img.height = 200# insert image height in pixels as float or int (e.g. 305.5)
                img.width=200

                ws.row_dimensions[row+2].height = 160

                # set the width of the column
                ws.column_dimensions[get_column_letter(j+5)].width = 30
                ws.add_image(img)
               
            

        for i in range(1,16):
            for j in range(1,16):
                #print(get_column_letter(j)+str(i))
                ws.row_dimensions[i].height = 160

                # set the width of the column
                ws.column_dimensions[get_column_letter(j)].width = 30
        ws['A1'] = "Remark"
        ws['B1'] = "Type"
        ws['C1'] = "Rename"
        ws['D1'] = "Themes"
        ws['O1'] = "Myntra_product_list"

        
        wb.save(config.WORKBOOK_NAME)

    def main(self):
        theme_lists, image_list = self.get_images()
        self.write_in_sheet(theme_lists,image_list)

        

import re
import os
from tqdm.auto import tqdm
from nltk.stem import PorterStemmer
from nltk.tokenize import word_tokenize
from phrase_chunking import PhraseChunking
from preprocessing import Preprocessing
import config as config
import nltk
import numpy as np
from embeddings import UniversalSentEnc
from sklearn.metrics.pairwise import cosine_similarity
import pandas as pd
nltk.download('punkt')
import logging
import json
from collections import Counter
from postprocessing import Postprocessing
import numpy as np
logging.basicConfig(level=logging.INFO, filename=config.LOGGER_PATH, filemode='w')

class Driver:

    def __init__(self):
        self.preprocessing = Preprocessing()
        self.phraseChunking = PhraseChunking()
        self.use = UniversalSentEnc()
        self.postprocessing_additional = Postprocessing()


    def read_caption(self):
        data = pd.read_csv(config.CRAWLED_DATA_PATH, low_memory=False)
        caption_list = list(set(list(data['description'])) | set(list(data['title'])))
        caption_list = [x for x in caption_list if str(x) != 'nan']
        brand_name = list(set(list(data['brand_name'])))
        brand_name = [x for x in brand_name if str(x) != 'nan']
        brand_name = [x.lower() for x in brand_name]
        return caption_list, brand_name


    '''def read_seed_themes(self):
        with open(config.SEED_THEMES_PATH,'r') as f:
            fashionwords = f.readlines()
        fashionwords = [x.lower()[:-1] for x in fashionwords]
        return fashionwords'''

    def read_themes(self, mode):
        
        if mode == "seed":
            with open(config.SEED_THEMES_PATH,'r') as f:
                themes = json.load(f)["seed_list"]
            #fashionwords = [x.lower()[:-1] for x in fashionwords]
        if mode == "already_existing":
            with open(config.ALREADY_EXISTING_THEMES,'r') as f:
                themes = json.load(f)["already_existing_list"]
            
        return themes

    
  
    def removeElements(self,lst, k):
        counted = Counter(lst)

        temp_lst = []
        for el in counted:
            if counted[el] < k:
                temp_lst.append(el)

        res_lst = []
        for el in lst:
            if el not in temp_lst:
                res_lst.append(el)

        return(res_lst)

    def candidate_generation_for_corpus(self, caption_list):
        list_of_candidate = []
        for sentence in tqdm(caption_list):
          cleaned_text = self.preprocessing.clean_text(sentence)
          list_of_chunked_candidate = self.phraseChunking.candidate_generation(cleaned_text)
          try:
            for chunked_candidate in list_of_chunked_candidate:
              list_of_candidate.append(chunked_candidate)
          except:
              pass
        list_of_candidate = self.removeElements(list_of_candidate, 20)
        list_of_candidate = list(set(list_of_candidate))
        logging.info("length_of_list_of_candidate {a}".format(a=len(list_of_candidate)))
        return list_of_candidate


    def remove_seed_themes(self, list_of_candidate, fashionwords, brand_name):
        #remove seed themes from list_of_chunked_candidate
        list_of_candidate = set(list_of_candidate) - set(fashionwords)

        stemmed_fashion_word = []
        ps = PorterStemmer()

        #for phrase in set(attribute_value + article_type + fashionwords):
        for phrase in set(fashionwords):
          phrase = phrase.lower()
          words = word_tokenize(phrase)
          string_list = []
          for w in words:
            string_list.append(ps.stem(w))

          stemmed_fashion_word.append(' '.join(string_list))

        #stemming list_of_candidate
        unstemmed_phrase = []
        stemmed_phrase = []
        for i, phrase in enumerate(list_of_candidate):
          unstemmed_phrase.append(phrase)

          words = word_tokenize(phrase)
          string_list = []
          for w in words:
            string_list.append(ps.stem(w))

          stemmed_phrase.append(' '.join(string_list))


        list_to_send_2 = [] #contains unstemmed phrase
        for i, phrase in enumerate(stemmed_phrase):
          if phrase  not in stemmed_fashion_word:
            list_to_send_2.append(unstemmed_phrase[i])


        list_to_send_3 = []
        for ele in list_to_send_2:
          duplicate = False
          word_list =  ele.split(" ")
          if len(word_list) == 2 and ps.stem(word_list[0]) == ps.stem(word_list[1]) :

            pass

          elif len(word_list) == 3 :
            if  ps.stem(word_list[0]) == ps.stem(word_list[1]) or ps.stem(word_list[1]) == ps.stem(word_list[2]) or ps.stem(word_list[0]) == ps.stem(word_list[2]):
              pass
            else:
              is_alpha = any(chr.isdigit() for chr in ele)
              if is_alpha == False:
                list_to_send_3.append(ele)

          else:
            is_alpha = any(chr.isdigit() for chr in ele)
            if is_alpha == False:
              list_to_send_3.append(ele)

        list_to_send_4 = []
        for ele in list_to_send_3:
          word_list = ele.split()
          ele_to_append =""
          for word in word_list:
            indicator = False
            for b in brand_name:
              if b in word:
                indicator = True
            if indicator == False:
              ele_to_append = ele_to_append + word+ " "
          list_to_send_4.append(ele_to_append.strip())

        #logging.info("list_to_send", set(list_to_send_4))
        list_to_send_4 = list(set(list_to_send_4))
        list_to_send_4 = [x for x in list_to_send_4 if len(x.split())>1]
        return list(set(list_to_send_4))


    def find_top_n_similar(self, list_of_candidate, resulting_embedding, fashionwords, fashion_embedding):
        
        #list_of_candidate = 
        #resulting_embedding = 
        
        sim_matrix = cosine_similarity(fashion_embedding, resulting_embedding)
        n = -config.TOP_K
        n_most_similar = np.argsort(sim_matrix,axis=1)[:,n:]
        n_most_cosine = np.sort(sim_matrix,axis=1)[:,n:]
        fashion_vocab = [[word] * -n for word in fashionwords]
        fashion_vocab = [x for l in fashion_vocab for x in l]
        index_list = [x for sublist in n_most_similar.tolist() for x in sublist]
        value_list = [x for sublist in n_most_cosine.tolist() for x in sublist]

        keywords = []
        just_keywords = []
        for i,index in enumerate(index_list):
          keywords.append((list_of_candidate[index],value_list[i],fashion_vocab[i]))
          just_keywords.append(list_of_candidate[index])
          expanded_set = list(set(just_keywords))
        #logging.info(keywords)
        expanded_set_embedding = self.use.create_embedding(expanded_set)
        sim_matrix = cosine_similarity(fashion_embedding, expanded_set_embedding)
        average = np.mean(sim_matrix, axis=0)
        n = config.TOP_N
        indexes = average.argsort()[::-1][:n]
        expanded_set_list = []
        for index in indexes:
            expanded_set_list.append(expanded_set[index])
        return expanded_set_list
    
    def remove_already_existing(self, list_of_candidate,resulting_embedding, list_of_already_existing, already_existing_embedding):
        print("len of list_of_candidate", len(list_of_candidate))
        print("shape_of_resulting_candidate", resulting_embedding.shape)
        sim_matrix = cosine_similarity(already_existing_embedding, resulting_embedding)
        #max_values = sim_matrix.max(axis=0, keepdims=True)
        #max_index = sim_matrix.argmax(axis=0)
        index_matrix = np.argwhere(sim_matrix > 0.95)
        discarded_index = index_matrix[:,1]
        discarded_index = list(set(discarded_index))
        resulting_embedding = np.delete(resulting_embedding, discarded_index , axis = 0)
        list_of_new_candidate = []
        for i, ele in enumerate(list_of_candidate):
            if i not in discarded_index:
                list_of_new_candidate.append(ele)
        print("len of new list_of_candidate", len(list_of_new_candidate))
        print("shape_of_new_resulting_candidate", resulting_embedding.shape)
        return list_of_new_candidate, resulting_embedding



    def postprocessing(self, expanded_set_list):
      
      embedding_expanded = self.use.create_embedding(expanded_set_list)
      sim_matrix_expanded = cosine_similarity(embedding_expanded, embedding_expanded)
      accepted = []
      rejected = []
      for i,row in enumerate(sim_matrix_expanded):
        for index,item in enumerate(row):
          if item > 0.93 and item < 0.99:
            #logging.info(expanded_set_list[i],expanded_set_list[index],item)
            rejected.append([expanded_set_list[i],expanded_set_list[index]])
          elif i == index:
            accepted.append(expanded_set_list[index])
          else:
            pass


      rejected_flat_list = [y for x in rejected for y in x]
      partially_accepted = [x[0] for x in rejected]
      accepted = set(accepted) - set(rejected_flat_list)
      final_accepted = list(set(accepted) | set(partially_accepted))
      return final_accepted


    '''def write_expanded_list(self, expanded_set_list):
        with open(config.EXPANDED_SET_PATH,"w") as f:
            for value in expanded_set_list:
                f.write(str(value) + "\n")'''

    def write_expanded_list(self, expanded_set_list):
        json_obj = {}
        json_obj["Expanded_list"] = expanded_set_list
        with open(config.EXPANDED_SET_PATH,"w") as f:
            json.dump(json_obj,f)



    def main(self):

        fashionwords = self.read_themes("seed")
        caption_list, brand_name = self.read_caption()
        print("generating_candidates")
        list_of_candidate = self.candidate_generation_for_corpus(caption_list)
        print("1",len(list_of_candidate))
        list_of_candidate = self.remove_seed_themes(list_of_candidate, fashionwords, brand_name)
        print("2",len(list_of_candidate))
        list_of_already_existing = self.read_themes("already_existing")
        self.use.use_embedding(list_of_candidate,fashionwords)

        with open(config.CANDIDATE_EMBEDDING_PATH, 'rb') as f:
            resulting_embedding = np.load(f)

        with open(config.SEED_THEMES_EMBEDDING_PATH, 'rb') as f:
            fashion_embedding = np.load(f)
            
        
        already_existing_embedding = self.use.create_embedding(list_of_already_existing)
        
        if os.path.exists(config.ALREADY_EXISTING_THEMES):
            list_of_candidate ,resulting_embedding = self.remove_already_existing(list_of_candidate,resulting_embedding, list_of_already_existing, already_existing_embedding)
        print("3",len(list_of_candidate) )   
        expanded_set_list = self.find_top_n_similar(list_of_candidate ,resulting_embedding, fashionwords, fashion_embedding)
        expanded_set_list = self.postprocessing(expanded_set_list)
        print(len(expanded_set_list))
        expanded_set_list = self.postprocessing_additional.remove_theme_containing_body_part_in_beg(expanded_set_list)
        print(len(expanded_set_list))
        self.write_expanded_list(expanded_set_list)
        logging.info("finished")

driver = Driver()
driver.main()

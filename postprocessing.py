import config as config
import json

class Postprocessing:
    
    
    '''def get_theme_list(self):       
        with open(config.EXPANDED_SET_PATH,'r') as f:
            theme_list = json.load(f)["Expanded_list"]
        return theme_list'''
    
    '''def get_theme_list(self):       
        
        return ["neck bag","sdfr","sdfr bhd","fghr"]'''
 
    
    def remove_repetitive_theme(self,theme_list):
        #theme_list = self.get_theme_list()
        #print(len(theme_list))
        themes_list = []
        for i,theme in enumerate(theme_list):
            indicator = False
            for j,t in enumerate(theme_list):
                #print(theme,t)
                if i != j:
                    #word_list = theme.split(' ')

                    if theme in t:
                        #print( theme+ "---"+t)
                        indicator = True
            if indicator == False:
                themes_list.append(theme)
        #print(themes_list)
        return themes_list
        
    def remove_theme_containing_body_part_in_beg(self,theme_list):
        final_cleaned_list = []
        body_parts = ["neck","leg","waist","shoulder"]
        themes_list = self.remove_repetitive_theme(theme_list)
        for theme in themes_list:
            if theme.startswith(tuple(body_parts)) == False:
                final_cleaned_list.append(theme)
        return final_cleaned_list
    
    
    
#postprocessing = Postprocessing()
#final_cleaned_list = postprocessing.remove_theme_containing_body_part_in_beg(["neck bag","sdfr","sdfr bhd","fghr"])
#print(final_cleaned_list)
#print(len(final_cleaned_list))

                
                
                